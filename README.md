# FEniCS Virtual Machines

This repository contains tools for *building* FEniCS
(<http://fenicsproject.org>) virtual machine images.

If you want to *run* FEniCS in a virtual machine, a lightweight image pre-built
using these tools is available at
(<http://fenicsproject/pub/virtual/fenics-latest.ova>).  It can be run using
VirtualBox (<http://www.virtualbox.org/>). It is ideal for classroom
environments and getting started with FEniCS.

## Contents

This repository contains scripts for building Open Virtualization Format (OVF)
images that can be run using virtual machine tools, e.g. VirtualBox
(<https://www.virtualbox.org/>). The scripts are located in the directory
`ovf/`. Further instructions are available in the file `ovf/README.md`.

# Support

Please report any issues with these instructions or the generated images at
<fenics-support@fenicsproject.org>. Specific issues can be registered on the
issue tracker at <https://bitbucket.org/fenics-project/fenics-virtual/issues>.

## Authors

Jack S. Hale (<jack.hale@uni.lu>),
Lizao Li (<lixx1445@umn.edu>),
Garth N. Wells (<gnw20@cam.ac.uk>).
